var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/",{
            templateUrl : "login.html",
            controller : "crud_Controller"
        })
        .when("/addpage", {
            templateUrl : "addpage.html",
            controller : "crud_Controller"
        })
        .when("/table", {
            templateUrl : "tabledata.html",
            controller : "crud_Controller"
        })
});
app.controller('crud_Controller',function ($scope,$location,$route) {
    $scope.EmpModel = {
        Id: 1,
        Firstname: '',
        LastName: '',
        Emailid: '',
        PhoneNo:  ''
    };
    $scope.EmpList = [];
    $('.update-btn').css('display','none');
    $('.cnl-btn').css('display','none');
    $('.edit-form').css('display','none');

    $scope.login = function(){
       var  loginInfo = {
            Emailid: $scope.EmpModel.Emailid,
            Password: $scope.EmpModel.Password
        };



        if(loginInfo.Emailid && loginInfo.Password !== ""){

            $location.url('/table');
            $.notify("Successfully Log In","success");
        }else{
            $.notify("Please enter the valid data","error");
        }

    };

    $scope.addData = function () {
        var empObj = {
            /* Id: $scope.EmpList.length + 1,*/
            Id: Date.now(),
            Firstname: $scope.EmpModel.Firstname,
            LastName: $scope.EmpModel.LastName,
            Emailid: $scope.EmpModel.Emailid,
            PhoneNo: $scope.EmpModel.PhoneNo
        };
        /*var f = $scope.EmpList.find('paresh');
        console.log(' EmpModel. ....//',f);*/
        console.log(' EmpModel. ....//',empObj.Firstname);
        console.log(' EmpModel. ....//',$scope.EmpList);
        console.log(' EmpModel. ....//',jQuery.inArray(empObj.Firstname, $scope.EmpList));

        /* $scope.EmpList.push(empObj);
         $.notify("Add Data Successfully","success");*/
        if(empObj.LastName && empObj.Firstname !== ""  && empObj.Emailid && empObj.PhoneNo !==''){
            $scope.EmpList.push(empObj);
            $.notify("Add Data Successfully","success");
            $scope.clearModel();
            localStorage.setItem("items", JSON.stringify($scope.EmpList));
            /*$location.path("table");*/
            $location.path('/table');
        }else{
            $.notify("Please enter the data","error");
        }
    };
    $scope.addNew = function () {
        $location.url('/addpage');
     };
    $scope.logout = function(){
        $location.url('/');
    };
    $scope.orderByMe = function(Emp) {
        $scope.myOrderBy = Emp;
    };
    $scope.updateData = function () {
      /*  $.grep($scope.EmpList, function (e) {*/
        angular.forEach($scope.EmpList, function (e) {
            if (e.Id == $scope.EmpModel.Id) {
                e.Firstname = $scope.EmpModel.Firstname;
                e.LastName = $scope.EmpModel.LastName;
                e.Emailid = $scope.EmpModel.Emailid;
                e.PhoneNo = $scope.EmpModel.PhoneNo;
            }
            localStorage.setItem("items", JSON.stringify($scope.EmpList));
        });

        $.notify("Update Data Successfully","success");
    };

    $scope.editData = function (employee) {
        $scope.EmpModel.Id = employee.Id;
        $scope.EmpModel.LastName = employee.LastName;
        $scope.EmpModel.Firstname = employee.Firstname;
        $scope.EmpModel.Emailid = employee.Emailid;
        $scope.EmpModel.PhoneNo = employee.PhoneNo;
        $('.add-btn').css('display','none');
        $('.update-btn').css('display','inline');
        $('.cnl-btn').css('display','inline');
        $('.add-form').css('display','none');
        $('.edit-form').css('display','block');
        localStorage.setItem("items", JSON.stringify($scope.EmpList));
        /*$location.url('editpage');*/
    };
    $scope.removeRow = function (d) {
        var cnf = confirm('Are you sure want to delete data ??');
        if(cnf === true){
            $scope.EmpList.splice(d,1);
        }else{
            return false;
        }
        $.notify("Delete data Successfully","error");
        localStorage.setItem("items", JSON.stringify($scope.EmpList));
    };
    $scope.clearModel = function () {
        $scope.EmpModel.Id = 0;
        $scope.EmpModel.LastName = '';
        $scope.EmpModel.Firstname = '';
        $scope.EmpModel.Emailid = '';
        $scope.EmpModel.PhoneNo ='';
        $('.add-btn').css('display','inline');
        $('.update-btn').css('display','none');
        $('.cnl-btn').css('display','none');
        $('.add-form').css('display','block');
        $('.edit-form').css('display','none');

    };

    $scope.css_form = {
        'border': '3px solid black',
        /*'padding': '17px 17px 8px 0px',*/
        'width': '30%',
        'text-align': '-webkit-left',
        'margin-left': '24%',
        'margin-top': '12px',
        'margin-bottom': '26px',
        'background-color': '#bcccff',
        'padding': '19px 21px 17px 12px',
        'border-radius': '13px',
        'top': '57%',
        'left': '41%',
        'margin-left': '-100px'

    };
    $scope.css_form_add = {
        'border': '3px solid black',
        /*'padding': '17px 17px 8px 0px',*/
        'width': '53%',
        'text-align': '-webkit-left',
        'margin-left': '24%',
        'margin-top': '12px',
        'margin-bottom': '26px',
        'background-color': '#bcccff',
        'padding': '19px 21px 17px 12px',
        'border-radius': '13px'
    };
    $scope.css_form2 = {
        'width' :'100%',
        'text-align': '-webkit-left',
        'margin-left': '0%',
        'margin-bottom': '26px',
        'background-color': 'rgb(158, 181, 255)',
       'padding': '19px 21px 17px 12px'
    };

    $scope.css_form_table = {
        'width':'100%',
        'margin-top': '2%'
    };
    $scope.init = function () {
        $scope.EmpList =  localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];
    };
    //$scope.initfun();
});
